package com.droidgo.communications;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Method;
import java.util.UUID;

import com.droidgo.R;

import lejos.nxt.Motor;
import lejos.nxt.remote.RemoteMotor;
//import lejos.nxt.Motor;
////import lejos
import lejos.robotics.BaseMotor;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.util.Log;

/**
 * The following class establishes a connection between the car and the Android device.
 * @author Ronan
 *
 */

public class CarDroidConnection {

	BluetoothAdapter bluetoothAdapter; //Android bluetooth adapter.
	BluetoothDevice car;
	BluetoothSocket socket;
	public static RemoteMotor motorA;
	public static RemoteMotor motorC;
	
	String carAddress = "00:16:53:1A:49:A0";
	
	public CarDroidConnection(Context context){
		
	}
	
	
	/**
	 * Check if the Anroid device has its bluetooth switched on, if not turn it on.
	 */
	public void configBluetooth(){
		bluetoothAdapter = BluetoothAdapter.getDefaultAdapter(); //Gets a handle on the default local device adapter.
		bluetoothAdapter.enable();
//		if(!bluetoothAdapter.isEnabled()){
//			bluetoothAdapter.enable();
//		}
	}
	
	public Boolean establishConnection(){
		Boolean result = false;
		car = bluetoothAdapter.getRemoteDevice(carAddress);
		
		try {
			
//			socket = car.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")); //Creates a socket for a secure outgoing connection to the NXT.
			
			Method method = car.getClass().getMethod("createRfcommSocket", new Class[] { int.class }); 
					socket = (BluetoothSocket) method.invoke(car, 1);
			
			socket.connect();
			result = true;
			
		} catch (Exception e) {
			Log.e("BLUETOOTH CONNECTION","The connection could not be established.");
			result = false;
		}
		return result;
	}
	
	public void sendMessage(byte message, String nxt) throws InterruptedException{
		BluetoothSocket carSocket = socket;
		
		if(carSocket !=null){
			try {
				OutputStreamWriter out = new OutputStreamWriter(carSocket.getOutputStream()); //Getting the output stream associated with the NXTs Bluetooth socket.
				out.write(message);
				out.flush(); //Ensures all bytes are written to the NXT.
				
				Thread.sleep(1500); //Wait for 1.5 seconds.
			} catch (IOException e) {
				
				e.printStackTrace();
			}
		}
		else{
			System.out.println("The NXT bluetooth connection was null.");
		}
	}
	
	public int receiveMessage(String nxt){
		BluetoothSocket carSocket = socket;
		int charac;
		
		if(carSocket != null){
			try{
				InputStreamReader in = new InputStreamReader(carSocket.getInputStream());
				charac = in.read();
				return charac;
			} catch (IOException e){
				
				e.printStackTrace();
				return -1;
			}
		}
		else{
			System.out.println("An error occured reading the message.");
			return -1;
		}
		
		
	}
	
	public void go(){
		motorA = new Motor().A;
        motorA.setPower(60);  
        motorA.backward();  
        
        motorC = new Motor().C;
        motorC.setPower(60);
        motorC.backward();
        
//		Motor.A.forward();
//		Motor.C.forward();
	}
	
	public void stop(){
		motorA.stop();
		motorC.stop();
//		Motor.A.stop();
//		Motor.C.stop();
	}
	
	
}
