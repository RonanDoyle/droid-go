package com.droidgo.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import com.droidgo.CarCommunications;
import com.droidgo.MainActivity;
import com.droidgo.R;

public class DeviceConnection extends Activity {
	
	private CarCommunications carCommunications;
	private Button connect;
	private EditText name;
	private EditText address;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.device_connection);
		
		name = (EditText) findViewById(R.id.device_name);
		address = (EditText) findViewById(R.id.device_address);
		connect = (Button) findViewById(R.id.device_connect);
		
//		connect.setOnClickListener((OnClickListener) this);
		
		carCommunications = new CarCommunications();
	}
	
	public void onClick(View v) {
		if(v.getId() == R.id.device_connect)
		{
			carCommunications.bluetoothConnection(name.getText().toString(), address.getText().toString());
			startActivity(new Intent(getApplicationContext(), MainActivity.class));
			finish();
		}	
	}
}
