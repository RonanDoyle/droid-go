package com.droidgo;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import lejos.nxt.addon.NXTLineLeader.Command;
import lejos.pc.comm.NXTCommFactory;
import lejos.pc.comm.NXTConnector;
import android.os.Handler;
import android.util.Log;

/**
 * This class will provide bluetooth connections to the NXT car.
 * @author Ronan
 *
 */

public class CarCommunications {
	
	private Handler handler; //User inferface message handler.
	private String TAG="CarCommunications";
	private NXTConnector connector;
	private DataInputStream dataInput;
	private DataOutputStream datOutput;
	private IncomingMessageMonitor incomingMessageMonitor;
	
	public CarCommunications(){
		
	}
	
	public CarCommunications(Handler handler){
		Log.d(TAG, "CarCommunications");
		this.handler = handler;
		incomingMessageMonitor = new IncomingMessageMonitor();
	}
	/**
	 * Used to connect to the NXT car.
	 * @return
	 */
	public boolean bluetoothConnection(String name, String address){
		Log.d(TAG, "Establishing a connection with " + name + " on bluetooth address " + address);
		connector = new NXTConnector();
		
		boolean connected = connector.connectTo(name, address, NXTCommFactory.BLUETOOTH); //Setting the protocol type to connect to.
		System.out.println("Connection established... " + connected);
		
		if(!connected){
			return connected;
		}
		
		dataInput = connector.getDataIn(); //Getting the data to input.
		datOutput = connector.getDataOut(); //Getting the outputed data.
		
		if(dataInput == null)
		{
			connected = false;
			return connected;
		}
		
		if(!incomingMessageMonitor.isActive)
		{
			incomingMessageMonitor.start();
		}
		
		return connected;
	}
	
	// @param The the array of floats (data) is built from the collection list parameters.
	public void action(Command command, float... data){
		while (incomingMessageMonitor.isActive)
		{
			Thread.yield(); //Causes the calling thread to yield execution to another thread which is ready to run.
		}
		try{
			datOutput.writeInt(command.ordinal()); //Converts the enumerator to an integer.
			for(float d: data)
			{
				datOutput.writeFloat(d);
			}
			datOutput.flush();
		}catch(IOException e){
			Log.e(TAG, "Action exception ", e);
		}
		incomingMessageMonitor.isActive = true;
	}
	
	/**
	 * This is a private class that is used to monitor the NXT for an incoming message after 
	 * a command has been issued to the car.
	 */
	
	class IncomingMessageMonitor extends Thread{
		public boolean incomingMessage = false;
		int count = 0;
		boolean isActive = false;
		public void run(){
			setName("Incoming message monitor thread."); //Sets the name of the thread.
			isActive = true;
			while(isActive)
			{
				if(incomingMessage)
				{
					Log.d(TAG, "Incoming message... ");
					float x = 0;
					float y = 0;
					float h = 0;
					boolean ok = false;
					try{
						// Taking in the positions of the robot.
						x = dataInput.readFloat();
						y = dataInput.readFloat();
						h = dataInput.readFloat();
						ok = true;
						Log.d(TAG, "data " + x + " " + y + " " + h);
					} catch (IOException e){
						Log.d(TAG, "Connection to the NXT has been lost.");
						count++;
						isActive = count < 20; //After 20 is reached, the monitor stops.
						ok = false;
					}
					if(ok)
					{
						incomingMessage = false;
					}
					try{
						Thread.sleep(30);
					} catch(InterruptedException ex)
					{
						//Logs the exception as Sever.
						Logger.getLogger(CarCommunications.class.getName()).log(Level.SEVERE, null, ex);
					}
				}
			}
		}
	}
}
