package com.droidgo;

import android.content.Context;
import android.view.View;
import android.webkit.WebView;

/**
 * The camera feed for the top section of the app. 
 * The camera will be mounted on the front of the DROID-GO car/u-GO.
 * @author Ronan
 *
 */

public class CameraFeed extends View {
	
	private WebView webView;
	 
	public CameraFeed(Context context)
	{
		super(context);
//		webView.getSettings().setJavaScriptEnabled(true);
//		webView.loadUrl("http://www.google.com");		
	}
}
