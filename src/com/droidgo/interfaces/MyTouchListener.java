package com.droidgo.interfaces;

import android.view.MotionEvent;

public interface MyTouchListener {
	
	public boolean onTouch(MotionEvent event);
//	public void setMyTouchListener(MyTouchListener myTouchListener);
//	public MyTouchListener getMyTouchListener();
}
