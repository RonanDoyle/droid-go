package com.droidgo.interfaces;

/**
 * The interface which listens for the joystick knob to be clicked.
 * @author Ronan
 *
 */

public interface KnobClickedListener {
	public void OnClicked();
    public void OnReleased();
}
