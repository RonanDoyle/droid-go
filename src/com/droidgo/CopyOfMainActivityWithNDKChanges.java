package com.droidgo;

import java.io.FileDescriptor;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.app.NativeActivity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Color;
import android.graphics.YuvImage;
import android.hardware.Camera;
import android.hardware.Camera.Parameters;
import android.hardware.Camera.PreviewCallback;
import android.hardware.Camera.Size;
import android.media.AudioManager;
import android.media.CamcorderProfile;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.ParcelFileDescriptor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.droidgo.comm.CarDroidConnection;
import com.droidgo.http2.NativeAgent;
import com.droidgo.http2.NetInfoAdapter;
import com.droidgo.http2.StreamingLoop;
import com.droidgo.http2.StreamingServer;
import com.droidgo.httpservice.LocalHttpService;
import com.droidgo.CameraView;


/**
 * The main activity for the project.
 * 
 * @author Ronan
 * 
 */

public class CopyOfMainActivityWithNDKChanges extends Activity implements OnTouchListener, PreviewCallback, Callback {

	private LinearLayout controlsLayout; // Section to contain the joystick.
	private LinearLayout cameraLayout; // Section to contain the camera feed.
	private TextView connectionStatus; // Displays connection status (Connected
										// or Disconnected).
	private int latchX, latchY = 0; // Used for refining the joystick control
									// mechanism.
	private Joystick joystick; // Joystick object.
	private CarDroidConnection carDroidCon; // Handles the connection between
											// the App and NXT.
	private SurfaceView preView;
	int cameraIndex = Camera.CameraInfo.CAMERA_FACING_FRONT;
	final Camera camera = Camera.open(cameraIndex);
	private SurfaceHolder previewHolder;
	private LocalHttpService mBoundService;
	private boolean mIsBound;
	
	 boolean inServer = false;
	    boolean inStreaming = false;
	
	private AudioManager audioManager = null; 
	private MediaRecorder mediaRecorder = null;
	Context c;
	
	CameraView myCamView;
    StreamingServer strServer;
    NativeAgent nativeAgent;
    StreamingLoop camLoop;
    StreamingLoop httpLoop;
    
    int tWidth = 320;
    int tHeight = 240;
    
    final String checkingFile = "/sdcard/droidgomedia/myvideo.mp4";
    final String resourceDirectory = "/sdcard/droidgomedia";

	private enum StateMachine { // ACTIVE used when user is touching app, IDLE
								// for when no touch exists
		ACTIVE, IDLE
	};

	private StateMachine state;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		carDroidCon = new CarDroidConnection(this);
		
		audioManager = (AudioManager)this.getSystemService(Context.AUDIO_SERVICE);
		audioManager.setStreamMute(AudioManager.STREAM_SYSTEM, false);
		
		controlsLayout = (LinearLayout) findViewById(R.id.controlsLayout);
		controlsLayout.setClickable(true);
		joystick = new Joystick(this);
		controlsLayout.addView(joystick);

		joystick.setOnTouchListener(this);

		connectionStatus = (TextView) findViewById(R.id.connection_status);
		connectionStatus.setBackgroundColor(Color.BLACK);
		connectionStatus.setText("Disconnected...");
		connectionStatus.setTextColor(Color.RED);

		controlsLayout.setBackgroundColor(Color.BLUE);
		
		SharedPreferences settings = getSharedPreferences("CAMPREFS", 0);
        LocalHttpService.setPwd(settings.getString("pwd", ""));
		String password = "droidgo";
		LocalHttpService.setPwd(password);
		Editor editor = settings.edit();
		editor.putString("password", password);
		editor.commit();
		
	    
	    final SurfaceView preview = (SurfaceView) findViewById(R.id.cameraView);
        previewHolder = preview.getHolder();
        previewHolder.addCallback(surfaceCallback);
//        myCamView = new CameraView(this, attr);
        previewHolder.addCallback(myCamView);
        previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS); //Needed for Android devices pre 3.0.
        camera.setDisplayOrientation(90);
        camera.setPreviewCallback(this);
		state = StateMachine.IDLE;
	}
	
	
	  @Override
	    public void onDestroy(){
	        super.onDestroy();
	    }

	    @Override
	    public void onStart(){
	        super.onStart();
	        setup();
	    }

	    @Override
	    public void onResume(){
	        super.onResume();
	    }

	    @Override
	    public void onPause(){      
	        super.onPause();
	        finish();
	        System.exit(0);
	    }
	
	
	 /**
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * @param width
	  * @param height
	  */
	
//	    private void showToast(Context context, String message) { 
//	        // create the view
//	        LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//	        View view = vi.inflate(R.layout.message_toast, null);
//
//	        // set the text in the view
//	        TextView tv = (TextView)view.findViewById(R.id.message);
//	        tv.setText(message);
//
//	        // show the toast
//	        Toast toast = new Toast(context);
//	        toast.setView(view);
//	        toast.setDuration(Toast.LENGTH_SHORT);
//	        toast.show();
//	    }  
//	
//	
//	 private void buildResources() {
//	        String[] str ={"mkdir", resourceDirectory};
//
//	        try { 
//	            Process ps = Runtime.getRuntime().exec(str);
//	            try {
//	                ps.waitFor();
//	            } catch (InterruptedException e) {
//	                e.printStackTrace();
//	            } 
//	        
//	            copyResourceFile(R.raw.index, resourceDirectory + "/index.html"  );
//	            copyResourceFile(R.raw.style, resourceDirectory + "/style.css"  );
//	            copyResourceFile(R.raw.player, resourceDirectory + "/player.js"  );
//	            copyResourceFile(R.raw.player_object, resourceDirectory + "/player_object.swf"  );
//	            copyResourceFile(R.raw.player_controler, resourceDirectory + "/player_controler.swf"  ); 
//	        }
//	        catch (IOException e) {
//	            e.printStackTrace();
//	        }
//	    }
//	 
//	 private void copyResourceFile(int rid, String targetFile) throws IOException {
//	        InputStream fin = ((Context)this).getResources().openRawResource(rid);
//	        FileOutputStream fos = new FileOutputStream(targetFile);  
//
//	        int     length;
//	        byte[] buffer = new byte[1024*32]; 
//	        while( (length = fin.read(buffer)) != -1){
//	            fos.write(buffer,0,length);
//	        }
//	        fin.close();
//	        fos.close();
//	    }
//	 
//	 private void clearResources() {
//	        String[] str ={"rm", "-r", resourceDirectory};
//
//	        try { 
//	            Process ps = Runtime.getRuntime().exec(str);
//	            try {
//	                ps.waitFor();
//	            } catch (InterruptedException e) {
//	                e.printStackTrace();
//	            } 
//	        } catch (IOException e) {
//	            e.printStackTrace();
//	        }
//	    }
	 
	 private void setup()
	 {
//		 clearResources();
//		 buildResources();
		 
//		 NativeAgent.LoadLibraries();
//		 nativeAgent = new NativeAgent();
//		 camLoop = new StreamingLoop("com.droidgo");
//		 httpLoop = new StreamingLoop("com.droidgo.http2");
		 
//		 myCamView = (CameraView) findViewById(R.id.surface_overlay);
//		 SurfaceView surfView = (SurfaceView) findViewById(R.id.cameraView);
//		 myCamView.SetupCamera(surfView);
		 
		 SurfaceView preview = (SurfaceView) findViewById(R.id.cameraView);
		 previewHolder = preview.getHolder();
//		 previewHolder.addCallback(surfaceCallback);
		 myCamView = new CameraView(this);
		 
		 myCamView.SetupCamera(preview); //Problem? Removing allows a step further ;)
//		 mBoundService.set
		 
		 
//       previewHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS); //Needed for Android devices pre 3.0.
//       camera.setDisplayOrientation(90);
//       camera.setPreviewCallback(this);
		 
	 }
	 
//	 private void startServer()
//	 {
//		 inServer = true;
////		 NetInfoAdapter.Update(this);
////		 inStreaming = true;
//		 System.out.println("IP ADDRESS ************** "+ NetInfoAdapter.getInfo("IP") + ":8080");
//	 
//		 try{
//			 strServer = new StreamingServer(8080, resourceDirectory);
//			 strServer.setOnRequestListen(streamReq);
//		 }catch (IOException e)
//		 {
//			 e.printStackTrace();
//		 }
////		 startStreaming();
//	 }
//		public void onPreviewFrame(byte[] data, Camera camera) {
//			Camera.Parameters params = camera.getParameters();
//			Size size = params.getPreviewSize();
//			final YuvImage image = new YuvImage(data, params.getPreviewFormat(), size.width, size.height, null);
//			mBoundService.setImage(image);
//		}
		
	 
	 private void action()
	 {
		 if( inServer == false)
		 {
			 myCamView.PrepareMedia(tWidth, tHeight);
			 boolean ret = myCamView.StartRecording(checkingFile);
			 
			 if(ret)
			 {
				 new Handler().postDelayed(new Runnable(){
					 public void run()
					 {
//						 myCamView.StopMedia();
//						 if(NativeAgent.NativeCheckMedia(tWidth, tHeight, checkingFile))
//						 {
							 startServer();
//						 } else{
//							 showToast(MainActivity.this, "There is an error with preparing, please try again");
//						 }
					 }
				 }, 2000);
			 } else {
//				 showToast(this, "Your phone do not support MP4+AMR_BN format, please exit by menu.");
			 }
		 } else {
			 stopServer();
		 }
	 }
	 
	 private void stopServer()
	 {
		 inServer = false;
		 if(strServer != null)
		 {
			 strServer.stop();
			 strServer = null;
		 }
	 }
	 
	 
	 private boolean startStreaming()
	 {
		 if(inStreaming == true)
		 {
			 return false;
		 }
		 camLoop.InitLoop();
		 httpLoop.InitLoop();
		 
		 System.out.println(camLoop.getReceiverFileDescriptor().toString());
		 System.out.println(httpLoop.getSenderFileDescriptor().toString());
		 
//		 NativeAgent.NativeStartStreamingMedia(camLoop.getReceiverFileDescriptor(), httpLoop.getSenderFileDescriptor());
		 
//		 myCamView.PrepareMedia(tWidth, tHeight);
		 boolean ret = myCamView.StartStreaming(camLoop.getSenderFileDescriptor());
		 if( ret == false)
		 {
			 return false;
		 }
		 
		 inStreaming = true; ///////**********************************
		 return true;
	 }
	 
	 private void stopStreaming()
	 {
		  if ( inStreaming == false)
	            return;
	        inStreaming = false;

	        myCamView.StopMedia(); 
	        httpLoop.ReleaseLoop();
	        camLoop.ReleaseLoop();
	        
	        nativeAgent.NativeStopStreamingMedia();
	 }
	
	 StreamingServer.OnRequestListen streamReq = new StreamingServer.OnRequestListen() {
	        @Override
	        public InputStream onRequest() {
	        	System.out.println("Request live streaming...");
	            Log.d("DROID-GO", "Request live streaming...");
	            if ( startStreaming() == false)
	                return null;
	            try {
	                InputStream ins = httpLoop.getInputStream(); 
	                return ins;
	            } catch (IOException e) {
	                e.printStackTrace();
	                Log.d("DROID-GO", "call httpLoop.getInputStream() error");
	                stopStreaming();              
	            } 
	            Log.d("DROID-GO", "Return a null response to request");
	            return null;
	        }
	        
	        @Override
	        public void requestDone()
	        {
	        	Log.d("DROID-GO", "Request for live streaming complete");
	        	stopStreaming();
	        }
	 };

	 /**
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * 
	  * @param width
	  * @param height
	  */
	
//	private void initPreview(int width, int height) {
//    	try {
//			camera.setPreviewDisplay(previewHolder);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//         
//    }
//      
//      private void startPreview() {
//    	  Parameters params = camera.getParameters();
//          params.setPreviewFpsRange(1, 150); //setPreviewFrameRate() depreciated for FpsRange();
//          camera.setParameters(params);
//          camera.startPreview();
//      }
	
      SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {
  		
  		public void surfaceDestroyed(SurfaceHolder holder) {
  			// TODO Auto-generated method stub
  			
  		}
  		
  		public void surfaceCreated(SurfaceHolder holder) {
  			// TODO Auto-generated method stub
  			
  		}
  		
  		public void surfaceChanged(SurfaceHolder holder, int format, int width,
  				int height) {
//  			initPreview(width, height);
//  			startPreview();
  			
  		}
  	};
	
	

	private void activeEvent(MotionEvent event) {
		System.out.println("LATCHX: " + latchX);
		System.out.println("LATCHY: " + latchY);
		
		
		
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			break;

		case MotionEvent.ACTION_UP:
			// Stopping motors.
			state = StateMachine.IDLE;
			carDroidCon.issueCommand(CarDroidConnection.MOTOR_A_C_STOP, 0);
			carDroidCon.issueCommand(CarDroidConnection.MOTOR_B_CENTER, 0);
			latchX = 0;
			latchY = 0;
			System.out.println("LATCH X: " + latchX);
			System.out.println("LATCH Y: " + latchY);
			break;

		case MotionEvent.ACTION_MOVE:

			// forward and reverse
			forwardReverse((int) event.getY());

			// left and right
			leftRight((int) event.getX());
			break;

		default:
			break;

		}
		// System.out.println("Current State: ACTIVE, Event: " + event
		// + ", New State: " + state);
	}

	private void idleEvent(MotionEvent event) {
		// The app does not send anything to server, instead it waits for the state to change to active.
		switch (event.getAction()) {
		case MotionEvent.ACTION_DOWN:
			state = StateMachine.ACTIVE;
			break;

		case MotionEvent.ACTION_UP:
			break;

		case MotionEvent.ACTION_MOVE:
			break;

		default:
			// System.out.println("ERROR: Incorrect Event " + event);
			break;

		}
		// System.out.println("Current State: ACTIVE, Event: " + event
		// + ", New State: " + state);
	}

	public boolean onTouch(View v, MotionEvent event) {
		boolean result = false;
		if (v == joystick) {
			switch (state) {
			case IDLE:
				idleEvent(event);
				break;

			case ACTIVE:

				activeEvent(event);
				break;

			default:
				// System.out.println("Error: Incorrect State " + state);
			}
		}
		return result;
	}

	private void forwardReverse(int y) {

		/**
		 * The following x,y values are hardcoded for a HTC Flyer's screen
		 * dimensions. Tests have not been properly carried out on other
		 * devices.
		 * 
		 * Will more than likely need to change these values to be dynamic,
		 * based on the devices screen size.
		 */

		// forward
		if (y < 187) {
			if (latchY == 0) {
				carDroidCon
						.issueCommand(CarDroidConnection.MOTOR_A_BACKWARD, 0);
				carDroidCon
						.issueCommand(CarDroidConnection.MOTOR_C_BACKWARD, 0);
				latchY = 1;
				new SendToTrim("FORWARD").execute();
			}
		}
		// reverse
		else if (y > 267) {
			if (latchY == 0) {
				carDroidCon.issueCommand(CarDroidConnection.MOTOR_A_FORWARD, 0);
				carDroidCon.issueCommand(CarDroidConnection.MOTOR_C_FORWARD, 0);
				latchY = 1;
				new SendToTrim("BACK").execute();
			}
		}
		// center
		else if (y >= 187 && y <= 267) {
			latchY = 0;
		}
	}

	private void leftRight(int x) {

		/**
		 * The following x,y values are hardcoded for a HTC Flyer's screen
		 * dimensions. Tests have not been properly carried out on other
		 * devices.
		 * 
		 * Will more than likely need to change these values to be dynamic,
		 * based on the devices screen size.
		 */

		// left
		if (x < 260) {
			if (latchX == 0) {
				carDroidCon
						.issueCommand(CarDroidConnection.MOTOR_B_BACKWARD, 0);
				latchX = 1;
				new SendToTrim("LEFT").execute();
			}
		}
		// right
		else if (x > 340) {
			if (latchX == 0) {
				carDroidCon.issueCommand(CarDroidConnection.MOTOR_B_FORWARD, 0);
				latchX = 1;
				new SendToTrim("RIGHT").execute();
			}
		}
		// center
		else if (x >= 260 && x <= 340) {
			latchX = 0;
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.connect:
//			connect();
//			doBindService();
			action();
//			try {
//				Thread.sleep(2000);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
			connectionStatus.setText("Connected...");
			connectionStatus.setTextColor(Color.GREEN);

			break;
		case R.id.disconnect:
//			disconnect();
//			doUnbindService();
			stopServer();
			connectionStatus.setText("Disconnected...");
			connectionStatus.setTextColor(Color.RED);

			// Following code is only needed for initial pairing between NXT and
			// Android device? Do Not Delete...yet!!!
			// **************************************************************************
			// carDroidCon.configBluetooth();
			// carDroidCon.establishConnection();
			// connector = CarDroidConnection.connect(CONN_TYPE.LEGO_LCP);
			// connector.connectTo("00:16:53:1A:49:A0");
			// if(connector.connectTo())
			// {
			//
			// }
			// startActivity(new Intent(getApplicationContext(),
			// DeviceConnection.class));
			// **************************************************************************
			break;
		}
		return true;
	}

//	public void connect() {
//		// carDroidCon.configBluetooth();
//		// carDroidCon.establishConnection();
//		// connector = CarDroidConnection.connect(CONN_TYPE.LEGO_LCP);
//		// connector.connectTo("00:16:53:1A:49:A0");
//		if (carDroidCon.socket == null) {
//			carDroidCon.configBluetooth();
//		} else {
//			carDroidCon.closeBluetoothConnection();
//		}
//	}
//
//	public void disconnect() {
//		carDroidCon.closeBluetoothConnection();
//	}

//	public void streamAudio()
//	{
//		String host = "10.20.100.50";
//		int port = 80;
//		Socket audioSocket = null;
//		
//		try{
//			audioSocket = new Socket(InetAddress.getByName(host), port);
//		} catch (UnknownHostException e)
//		{
//			System.out.println("Unknown Host Exception");
//			e.printStackTrace();
//		} catch (IOException e)
//		{
//			e.printStackTrace();
//		}
//		
//		ParcelFileDescriptor parcelfd = ParcelFileDescriptor.fromSocket(audioSocket);
//		
//		MediaRecorder recorder = new MediaRecorder();
//		recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
//		recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
//		recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
//		recorder.setOutputFile(parcelfd.getFileDescriptor());
//		
//		try
//		{
//			recorder.prepare();
//		} catch (IllegalStateException e)
//		{
//			e.printStackTrace();
//		} catch (IOException e)
//		{
//			e.printStackTrace();
//		}
//		mBoundService.setAudio(recorder);	
//	}
	
	

	public void onPreviewFrame(byte[] data, Camera camera) {
		Camera.Parameters params = camera.getParameters();
		Size size = params.getPreviewSize();
		final YuvImage image = new YuvImage(data, params.getPreviewFormat(), size.width, size.height, null);
		mBoundService.setImage(image);
	}
	
//	private ServiceConnection conn = new ServiceConnection()
//	{		
//		public void onServiceConnected(ComponentName name, IBinder service) {
//			mBoundService = ((LocalHttpService.LocalBinder)service).getService();
//			
//		}
//		public void onServiceDisconnected(ComponentName name) {
//			mBoundService = null;
//			
//		}
//	};
//	
//	public void doBindService()
//	{
//		Intent mServiceIntent = new Intent(this, LocalHttpService.class);
//		bindService(mServiceIntent, conn, Context.BIND_AUTO_CREATE);
//		mIsBound = true;
//	}
//	
//	public void doUnbindService()
//	{
//		if(mIsBound)
//		{
//			unbindService(conn);
//			mIsBound = false;
//		}
//	}
//	  @Override
//	    protected void onDestroy() {
//	        camera.setPreviewCallback(null);
//	        camera.release();
//	        doUnbindService();
//	    	
//	    	super.onDestroy();
//	    }

	


	
	private class SendToTrim extends AsyncTask<Void, Integer, Void> {

		String message;
		
	    	

			public SendToTrim(String msg)
	    	{
	    		setServer(msg);
	    	}
	    	
	    	@Override
			protected void onPreExecute() {
				// update the UI immediately after the task is executed
				super.onPreExecute();
				
			}
	    	
			@Override
			protected Void doInBackground(Void... params) {
				
			  	    HttpClient httpclient = new DefaultHttpClient();
			  	  HttpPost httppost = new HttpPost("http://10.20.100.50/AndroidTest/testapp.php"); //Mac
//			   	    HttpPost httppost = new HttpPost("http://192.168.1.2/AndroidTest/testapp.php"); //Mac
//			  	    HttpPost httppost = new HttpPost("http://192.168.1.9/TestFolder/testapp.php"); //Laptop
//			  	    HttpPost httppost = new HttpPost("http://10.8.80.73/testapp.php"); //Trim Slice
			   	 try {
			   	   List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			       nameValuePairs.add(new BasicNameValuePair("id", "5656"));
			       nameValuePairs.add(new BasicNameValuePair("message", getServer()));
			       httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			   	   httpclient.execute(httppost);
//			   	   streamAudio();
			   	   
			     } catch (ClientProtocolException e) {
			         System.out.println(e);
			     } catch (IOException e) {
			         System.out.println(e);
			     }
		      	 
				return null;
			}

			
			@Override
			protected void onPostExecute(Void result) {
				super.onPostExecute(result);

			}
			
			public String getServer() {
				return message;
			}

			public void setServer(String server) {
				this.message = server;
			}
	}





	@Override
	public void surfaceChanged(SurfaceHolder arg0, int arg1, int arg2, int arg3) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
	}




	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
	}
}
	 